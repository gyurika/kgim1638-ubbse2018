# CC (Catering company)

    A catering company shipping food to companies on workdays. 
    The caterer offers customer services.
    Online platform for companies to easily register and choose menus to be shipped at a selected time interval.
    Employees working for the catering company can mange the the orders and menus by the application.

## Prerequisites

    download Node.js v11.0.0 and npm 6.4.1 [https://www.npmjs.com/get-npm]
    download MySQL Workbench 8.0.12 [https://www.mysql.com/products/workbench/]
    
## Installing

    ....

## Built with

    Javascript, Node.js (CLI, Sequelizes, React Redux)

## Author

    Kőszegi György

